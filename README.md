# Mailer
**Functionality:**

+ receive JMS-message from activeMQ server 
+ add documents to mongodb 
+ send email notifications to users of SocialNetwork 

**Tools:**
JDK 8, Spring Boot, jms/activeMQ, MongoDB, Spring Data, Maven, JavaMailSender, IntelliJIDEA   
_  
**Ibragimov Viner**  
Training getJavaJob  
http://www.getjavajob.com  