package com.getjavajob.training.mailer;

import com.getjavajob.training.mailer.common.Notification;
import com.getjavajob.training.mailer.dao.NotificationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

import static com.getjavajob.training.mailer.ActiveMQConfig.ORDER_QUEUE;

@Component
public class Receiver {

    private final NotificationRepository notificationRepository;
    private final JavaMailSender emailSender;

    @Autowired
    public Receiver(NotificationRepository notificationRepository, JavaMailSender emailSender) {
        this.notificationRepository = notificationRepository;
        this.emailSender = emailSender;
    }

    @JmsListener(destination = ORDER_QUEUE)
    public void receiveMessage(JmsMessage jmsMessage) {
        System.out.println("Received <" + jmsMessage + ">");
        notificationRepository.save(new Notification(jmsMessage.getEmail(), jmsMessage.getText()));
        sendMail(jmsMessage);
    }

    private void sendMail(JmsMessage jmsMessage) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(jmsMessage.getEmail());
        message.setSubject("New notification");
        message.setText(jmsMessage.getText());
        emailSender.send(message);
    }

}
