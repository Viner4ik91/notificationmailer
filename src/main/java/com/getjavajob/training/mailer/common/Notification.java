package com.getjavajob.training.mailer.common;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.Objects;

import static java.util.Objects.hash;

@Document(collection = "Notification")
public class Notification {

    @Id
    private String id;

    @Field(value = "email")
    private String email;

    @Field(value = "msg_txt")
    private String text;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Notification() {
    }

    public Notification(String email, String text) {
        this.email = email;
        this.text = text;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Notification)) {
            return false;
        }
        Notification notification = (Notification) o;
        return Objects.equals(id, notification.id) &&
                Objects.equals(email, notification.email) &&
                Objects.equals(text, notification.text);
    }

    @Override
    public int hashCode() {
        return hash(id, email, text);
    }

    @Override
    public String toString() {
        return "Notification{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", text='" + text + '\'' +
                '}';
    }

}
