package com.getjavajob.training.mailer;

import java.io.Serializable;

public class JmsMessage implements Serializable {

    private String email;
    private String text;

    public JmsMessage() {
    }

    public JmsMessage(String email, String text) {
        this.email = email;
        this.text = text;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return "JmsMessage{" +
                "email='" + email + '\'' +
                ", text='" + text + '\'' +
                '}';
    }

}
