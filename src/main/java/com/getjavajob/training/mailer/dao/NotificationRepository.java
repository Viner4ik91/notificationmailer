package com.getjavajob.training.mailer.dao;

import com.getjavajob.training.mailer.common.Notification;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface NotificationRepository extends MongoRepository<Notification, Long> {

}
